from board import SCL, SDA
import busio
from adafruit_pca9685 import PCA9685
i2c_bus = busio.I2C(SCL,SDA)
pca = PCA9685(i2c_bus)
pca.frequency = 60

motor_names = {
    'motor1': 0,
    'motor2': 0
}

motor_value = 8600

nvolts = 6500
lvolts = 4330
hvolts = 8540

class DriveTrain:
    def __init__(self, leftChannel, rightChannel):
        self.leftChannel = leftChannel
        self.rightChannel = rightChannel

    def ArcadeDrive(self, speed, rotation):
        print(str(speed) + " " + str(rotation))


